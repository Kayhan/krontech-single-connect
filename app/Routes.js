import React from 'react';
import { Switch, Route } from 'react-router';
import routes from './constants/routes';
import App from './containers/App';
import HomePage from './containers/HomePage';
import LoginPage from './containers/LoginPage';
import ConnectionsPage from './containers/ConnectionsPage';
import withAuthentication from './components/Auth/withAuthentication';

export default () => (
  <App>
    <Switch>
      <Route path={routes.HOME} component={HomePage} />
      <Route path={routes.CONNECTIONS} component={ConnectionsPage} />
      <Route path={routes.LOGIN} component={LoginPage} />
    </Switch>
  </App>
);

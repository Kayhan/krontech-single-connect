// @flow
import type { GetState, Dispatch } from '../reducers/types';
import axios from 'axios';
import { ipcRenderer } from 'electron';

export const LOGIN = 'LOGIN';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT = 'LOGOUT';

export function login(username: string, password: string, server: string) {
  return async function (dispatch) {
    dispatch({
      type: LOGIN
    });
    axios.post(`http://${server}/aioc-rest-web/rest/login`, { username, password })
      .then(body => {
        if (body.headers["set-cookie"]) {
          axios.defaults.headers = { Cookie: body.headers["set-cookie"][0] };
          ipcRenderer.sendSync("cookie", { server, sessionId: body.data.sessionId });

        }
        dispatch({
          type: LOGIN_SUCCESS,
          sessionId: body.data.sessionId,
          server
        });
      }, (reason) => {
        dispatch({
          type: LOGIN_FAILURE,
          error: reason
        });
      });
  }
}

export function logout() {
  return {
    type: LOGOUT
  };
}
// @flow
import React, { Component } from 'react';
import Connections from '../components/Connections/Connections';

type Props = {};

export default class ConnectionsPage extends Component<Props> {
  props: Props;

  render() {
    return <Connections />;
  }
}

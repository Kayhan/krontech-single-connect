import { 
    LANGUAGE_CHANGE_SUCCESS,
    LANGUAGE_CHANGE_FAILURE,
    LANGUAGE_GET_SUCCESS,
    LANGUAGE_GET_FAILURE
    } from '../actions/localization';
import type { Action } from './types';

export default function language(state: object = { language: "en-US" }, action: Action) {
    switch (action.type) {
        case LANGUAGE_CHANGE_SUCCESS:
            return {
                ...state,
                language: action.language
            };
        case LANGUAGE_CHANGE_FAILURE:
            return {
                ...state,
                reason: action.reason
            }
        case LANGUAGE_GET_SUCCESS:
            return {
                ...state,
                language: action.language
            }
        case LANGUAGE_GET_FAILURE:
            return {
                ...state,
                reason: action.reason
            }
        default:
            return state;
    }
}



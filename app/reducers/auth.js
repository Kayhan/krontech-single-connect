// @flow
import { LOGIN, LOGOUT, LOGIN_SUCCESS, LOGIN_FAILURE } from '../actions/auth';
import type { Action } from './types';

export default function auth(state: object = { sessionId: "", auth: false, server: "" }, action: Action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        sessionId: action.sessionId,
        server: action.server,
        auth: true,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        error: action.error,
        auth: false,
      };
    case LOGOUT:
      return {
        ...state,
        sessionId: "",
        auth: false
      };
    default:
      return state;
  }
}
import type { Dispatch as ReduxDispatch, Store as ReduxStore } from 'redux';

export type authStateType = {
  counter: number,
  sessionId: string,
  auth: boolean,
  language: string
}

export type Action = {
  +type: string
};

export type GetState = () => authStateType;

export type Dispatch = ReduxDispatch<Action>;

export type Store = ReduxStore<GetState, Action>;

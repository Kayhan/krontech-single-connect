import React, { Component } from 'react';
import styles from './AddModal.css';
import { Formik } from 'formik';
import * as Yup from 'yup';

import saveImage from "../../../../images/save_button.png";
import cancelImage from '../../../../images/cancel_button.png';

const schema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too short!')
    .max(10, 'Too long!')
    .required('Required!'),
  address: Yup.string()
    .min(7, 'Too short!')
    .max(15, 'Too long!')
    .required('Required!'),
  port: Yup.string()
    .min(1, 'Too short!')
    .max(5, 'Too long!')
    .required('Required!'),
  ssl: Yup.bool(),
  cert: Yup.bool()
})

class AddModal extends Component {
  render() {
    const { handleAdd, handleEdit, closeModal, initialValues, type } = this.props;
    let values = {
      name: "",
      address: "",
      port: "",
      ssl: false,
      cert: false
    };
    if (initialValues && type !== 'Add') {
      values = initialValues;
    }
    return (
      <div className={styles.modal}>
        <div className={styles.title}>{`${type} Server`}</div>
        <div className={styles.form}>
          <Formik
            initialValues={values}
            validate={values => {
              let errors = {};
              return errors;
            }}
            validationSchema={schema}
            onSubmit={type === 'Add' ? handleAdd : handleEdit}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => (
                <div className={styles.wrapper}>
                  <form className={styles.form} onSubmit={handleSubmit}>
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="name">Name</label>
                      <input
                        className={styles.input}
                        type="text"
                        name="name"
                        id="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                    </div>
                    <div className={styles.error}>{errors.name}</div>
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="address">Address</label>
                      <input
                        className={styles.input}
                        type="text"
                        name="address"
                        id="address"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.address}
                      />
                    </div>
                    <div className={styles.error}>{errors.address}</div>
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="port">Port</label>
                      <input
                        className={styles.input}
                        type="text"
                        name="port"
                        id="port"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.port}
                      />
                    </div>
                    <div className={styles.error}>{errors.port}</div>
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="ssl">Use SSL</label>
                      <input
                        // className={styles.input}
                        type="checkbox"
                        name="ssl"
                        id="ssl"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.ssl}
                        defaultChecked={values.ssl}
                      />
                    </div>
                    <div className={styles.error}>{errors.ssl}</div>
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="cert">Ignore Certificate Errors</label>
                      <input
                        // className={styles.input}
                        type="checkbox"
                        name="cert"
                        id="cert"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.cert}
                        defaultChecked={values.cert}
                      />
                    </div>
                    <div className={styles.error}>{errors.cert}</div>
                    <div className={styles.buttons}>
                      <button className={styles.button} type="submit">
                        <img className={styles.buttonImage} src={saveImage} width='90%' />
                      </button>
                      <div className={styles.button}>
                        <img onClick={closeModal} className={styles.buttonImage} src={cancelImage} width='90%' />
                      </div>
                    </div>
                  </form>
                </div>
              )}
          </Formik>
        </div>
      </div >
    );
  }
}

export default AddModal;
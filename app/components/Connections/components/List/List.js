import React, { Component } from 'react';
import styles from './List.css';

export default class List extends Component {
  render() {
    const { addresses, handleSelect, selectedConnection } = this.props;
    return (
      <select value={selectedConnection} className={styles.select} size={2} onChange={handleSelect}>
        {
          addresses ? addresses.map((address, index) => {
            return <option className={styles.option} value={index} key={index}>{`${address.name + '-' + address.address + ':' + address.port}`}</option>;
          }) : null
        }
      </select>
    )
  }
}
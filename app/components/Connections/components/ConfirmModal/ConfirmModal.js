import React, {Component} from 'react';
import styles from './ConfirmModal.css';

export default class ConfirmModal extends Component{
    render(){
        const {handleConfirm, handleCancel} = this.props;
        return(
            <div className={styles.wrapper}>
                <div className={styles.message}>Are you sure?</div>
                <div className={styles.buttons}>
                    <button className={styles.button} onClick={handleConfirm}>Yes</button>
                    <button className={styles.button} onClick={handleCancel}>No</button>
                </div>
            </div>
        )
    }
}
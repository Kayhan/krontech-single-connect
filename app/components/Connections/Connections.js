import React, { Component } from 'react';
import { Link, Redirect, withRouter } from 'react-router-dom';
import storage from 'electron-json-storage';
import Modal from 'react-modal';

import routes from '../../constants/routes';
import List from './components/List/List';
import AddModal from './components/AddModal/AddModal';

import singleConnectLogo from '../../images/single_connect.png';
import addImage from '../../images/add_button.png';
import editImage from '../../images/edit_button.png';
import deleteImage from '../../images/delete_button.png';
import backImage from '../../images/back_icon.png';

import styles from './Connections.css';
import ConfirmModal from './components/ConfirmModal/ConfirmModal';

type Props = {};

class Connections extends Component<Props> {

  state = {
    selectedConnection: 0,
    addresses: [],
    modalIsOpen: false,
    type: "",
    confirmModalIsOpen: false
  }

  handleSelect = (event) => {
    this.setState({ selectedConnection: event.target.value });
  }

  initializeStorage = () => {
    // storage.clear();
    storage.get('addresses', (err, data) => {
      if (err) throw err;
      if (data.length >= 0) {
        this.setState({ addresses: data })
      }
    })
  }

  handleAdd = (values, { setSubmitting, setErrors }) => {
    storage.get('addresses', (err, data) => {
      if (err) throw err;
      data = data.length >= 0 ? data.concat([values]) : [values];
      storage.set('addresses', data, (err) => {
        if (err) throw err;
        this.initializeStorage();
        setSubmitting(false);
        this.closeModal();
      });
    });
  }

  handleEdit = (values, { setSubmitting }) => {
    let { addresses, selectedConnection } = this.state;
    addresses[selectedConnection] = values;
    storage.set('addresses', addresses, (err) => {
      if (err) throw err;
      this.initializeStorage();
      setSubmitting(false);
      this.closeModal();
    })
  }

  componentWillMount = () => {
    this.initializeStorage();
  }

  handleAddClicked = () => {
    this.setState({
      modalIsOpen: true,
      type: 'Add'
    });
  }

  handleEditClicked = () => {
    this.setState({
      modalIsOpen: true,
      type: 'Edit'
    });
  }

  handleDelete = () => {
    this.setState({
      confirmModalIsOpen: true
    })
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  handleReturnClicked = () => {
    this.props.history.goBack();
  }

  handleConfirm = () => {
    let { addresses, selectedConnection } = this.state;
    if(addresses.length>1)
    {
      addresses.splice(selectedConnection, 1);
      this.setState({selectedConnection: 0});
    }
    else if(addresses.length === 1)
    {
      addresses = [];
    }
    storage.set('addresses', addresses, (err) => {
      if (err) throw err;
      this.initializeStorage();
      this.handleCancel();
    });
  }

  handleCancel = () => {
    this.setState({
      confirmModalIsOpen: false
    });
  }

  render() {
    const btnWidth = 410 / 4;
    const { addresses, selectedConnection, type } = this.state;
    return (
      <div className={styles.wrapper}>
        <div className={styles.logo}>
          <img src={singleConnectLogo} />
        </div>
        <div className={styles.buttons}>
          <div className={styles.button} >
            <img onClick={this.handleAddClicked} src={addImage} width={btnWidth} />
          </div>
          <div className={styles.button}>
            <img onClick={this.handleEditClicked} src={editImage} width={btnWidth} />
          </div>
          <div className={styles.button}>
            <img onClick={this.handleDelete} src={deleteImage} width={btnWidth} />
          </div>
        </div>
        <div className={styles.list}>
          <List selectedConnection={selectedConnection} addresses={addresses} handleSelect={this.handleSelect} />
        </div>
        <div className={styles.returnWrapper}>
          <div onClick={this.handleReturnClicked} className={styles.returnButton}>
            <img src={backImage} width={btnWidth / 2} />
            <div>Return</div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          className={styles.modal}
        >
          <AddModal initialValues={addresses[selectedConnection]} 
            type={type} handleEdit={this.handleEdit} handleAdd={this.handleAdd} closeModal={this.closeModal} />
        </Modal>
        <Modal
          isOpen={this.state.confirmModalIsOpen}
          onRequestClose={this.handleCancel}
          className={styles.confirmModal}
        >
          <ConfirmModal handleConfirm={this.handleConfirm} handleCancel={this.handleCancel}/>
        </Modal>
      </div>
    );
  }
}

export default withRouter(Connections);
import React, { Component } from 'react';
import styles from './FavoritesTab.css';
import axios from 'axios';
import withLocalization from '../../../Localization/withLocalization';

import FavIcon from '../FavIcon/FavIcon';
import addIcon from '../../../../images/add_icon.png';

class FavoritesTab extends Component {

  state = {
    favFolders: [],
    devices: [],
    isFiltered: false,
    newFolderEditable: false
  }

  prevSearchText = "";

  componentWillMount = async () => {
    await this.getFavouriteDeviceGroupFolders();
  }

  componentDidUpdate = async () => {
    const { searchText } = this.props;
    await this.getDeviceGroups();
  }

  handleNewFolder = () => {
    this.setState({ newFolderEditable: true })
  }

  setNewFolder = async (folderName) => {
    axios.post("http://10.20.41.236/aioc-rest-web/rest/discovery/saveFavDeviceGroupFolders",
      {
        "dbId": "",
        "name": `${folderName}`
      })
      .then(res => {
        let data = res.data;

        this.setState(prevState => ({
          devices: prevState.devices.concat([data]),
          favFolders: prevState.favFolders.concat([data])
        }))

        this.setState(state => {
          const devices = state.devices.push
        })
      })
      .catch(err => { });
  }

  handleFolderSubmit = (event: KeyboardEvent) => {
    if (event.key === "Enter") {
      this.setNewFolder(event.target.value);
      this.setState({ newFolderEditable: false });
    }
    if (event.key === "Escape") {
      this.setState({ newFolderEditable: false });
    }
  }

  getDeviceGroups = async () => {
    const { searchText, server } = this.props;
    const { devices, isFiltered } = this.state;
    if (searchText === this.prevSearchText) return;
    if (searchText === "" && isFiltered) {
      this.setState({
        favFolders: devices,
        isFiltered: false
      });
      return;
    }
    this.prevSearchText = searchText;
    axios.get(`http://${server}/aioc-rest-web/rest/discovery/getFavouriteDeviceGroupFolders`)
      .then(res => {
        let folderData = res.data;
        axios.get(`http://${server}/aioc-rest-web/rest/discovery/searchDeviceByKeyword/${searchText}`)
          .then(res => {
            let data = res.data;
            let filteredFolders = folderData;
            filteredFolders.map(folder => {
              folder.devices = folder.devices.map(device => {
                if (data.filter(d => d.dbId === device.dbId).length === 0) return;
                return device;
              }).filter(d => d)
            });
            filteredFolders = filteredFolders.filter(f => f.devices.length > 0);
            this.setState({ favFolders: filteredFolders, isFiltered: true });
          })
          .catch(err => { })
      })
      .catch(err => { })
  }

  getFavouriteDeviceGroupFolders = async () => {
    const { server } = this.props;
    return axios.get(`http://${server}/aioc-rest-web/rest/discovery/getFavouriteDeviceGroupFolders`)
      .then(res => {
        let data = res.data;
        this.setState({ devices: data, favFolders: data });

      })
      .catch(err => { })
  }

  handleDelete = async (dbId) => {
    const { server } = this.props;
    return axios.delete(`http://${server}/aioc-rest-web/rest/discovery/deleteFavDeviceGroupFolders`, { data: { dbId } })
      .then(res => {
        this.setState(state => ({
          favFolders: state.favFolders.filter(f => f.dbId !== dbId)
        }))
      })
      .catch(err => { })
  }

  handleFav = (dbId) => {
    this.setState(state => ({
      favFolders: state.favFolders.map(f => {
        return {
          ...f,
          devices: f.devices ? f.devices.filter(d => d.dbId !== dbId) : [],
          deviceIds: f.deviceids ? f.deviceIds.filter(d => d !== dbId) : []
        }
      })
    }));
  }

  render() {
    const { favFolders, newFolderEditable } = this.state;
    const { server, texts } = this.props;
    return (
      <div className={styles.container}>
        {
          favFolders.map((folder, index) => {
            return (
              <DeviceGroupCollapse server={server} handleDelete={this.handleDelete} handleFav={this.handleFav} folder={folder} key={index} />
            );
          })
        }
        <div className={styles.newFolder} onClick={this.handleNewFolder}>
          <img className={styles.newFolderIcon} src={addIcon} />
          {
            newFolderEditable ?
              <input onKeyDown={this.handleFolderSubmit} onSubmit={this.handleFolderSubmit} autoFocus={true}></input> :
              <div>{texts.newFolder}</div>
          }
        </div>
      </div>
    )
  }
}

import arrowDown from '../../../../images/arrow_down.png';
import arrowRight from '../../../../images/arrow_right.png';
import { newWindow } from '../remoteHelpers';

class DeviceGroupCollapse extends Component {

  state = {
    expanded: true,
  }

  handleCollapse = (event: Event, dbId: string) => {
    const { handleDelete } = this.props;
    if (event.button === 2) {
      handleDelete(dbId);
      return;
    }
    if (event.button === 0) {
      this.setState(prevState => ({
        expanded: !prevState.expanded
      }));
    }
  }

  handleSelect = (event: Event, device) => {
    const { server } = this.props;
    event.stopPropagation();
    event.preventDefault();
    //TODO: Open new window
    newWindow(server, device);
  }

  handleClick = (event) => {
    event.stopPropagation();
    return false;
  }

  handleClickFav = (deviceId) => {
    this.props.handleFav(deviceId);
  }

  getCollapseIcon = () => {
    const { expanded } = this.state;
    return expanded ? arrowDown : arrowRight;
  }

  render() {
    const { expanded } = this.state;
    const { server, folder } = this.props;

    return (
      <div className={styles.deviceGroup} onClick={(event) => { this.handleCollapse(event, folder.dbId) }}>
        <div className={styles.title}><img src={this.getCollapseIcon()} />{folder.name}</div>
        {
          expanded && folder.devices ? folder.devices.map((device, index) => {
            return (
              <div className={styles.deviceContainer} onClick={this.handleClick} onDoubleClick={(event) => { this.handleSelect(event, device) }} key={index}>
                <div className={styles.device}>{device.name}</div>
                <FavIcon server={server} handleClick={this.handleClickFav} deviceId={device.dbId} favFolderId={folder.dbId} isFav={true} />
              </div>
            );
          }
          ) : null
        }
      </div>
    )
  }
}

export default withLocalization(FavoritesTab);

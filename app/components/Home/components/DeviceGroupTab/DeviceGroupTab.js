import axios from 'axios';
import React, { Component } from 'react';
import Modal from 'react-modal';
import arrowDownImage from '../../../../images/arrow_down.png';
import arrowRightImage from '../../../../images/arrow_right.png';
import FavIcon from '../FavIcon/FavIcon';
import FavModal from '../FavModal/FavModal';
import styles from './DeviceGroupTab.css';
const { BrowserWindow, session } = require('electron').remote;
import { ipcRenderer } from 'electron';
import { newWindow } from '../remoteHelpers';
class DeviceGroupTab extends Component {

  state = {
    deviceGroups: [],
    favDeviceIds: [],
    filteredIds: [],
    favFolders: [],
    isFiltered: false,
    isModalOpen: false,
    favDeviceId: ""
  }

  prevSearchText = "";

  componentWillMount = () => {
    this.initGetDeviceGroups();
    // this.getDeviceGroups();
    this.getFavouriteDeviceGroupFolders();
  }

  componentDidUpdate = async () => {
    await this.getSearchedDevices();
  }

  openModal = () => {
    this.setState({ isModalOpen: true });
  }

  closeModal = () => {
    this.setState({ isModalOpen: false });
  }

  getSearchedDevices = async () => {
    const { server, searchText } = this.props;
    const { isFiltered } = this.state;
    if (this.prevSearchText === searchText) return;
    this.prevSearchText = searchText;
    if (searchText === "" && isFiltered) {
      this.setState({ filteredIds: [], isFiltered: false });
    }
    axios.get(`http://${server}/aioc-rest-web/rest/discovery/searchDeviceByKeyword/${searchText}`)
      .then(res => {
        let data = res.data;
        let filteredIds = data.map(d => d.dbId);
        this.setState({ filteredIds, isFiltered: true });
      })
  }

  initGetDeviceGroups = async () => {
    const { server } = this.props;
    axios.get(`http://${server}/aioc-rest-web/rest/discovery/getDeviceGroupsOfSessionUser`)
      .then(res => {
        let data = res.data;
        this.setState({ devices: data, deviceGroups: data });
      })
      .catch(err => { console.log(err) })
  }

  getFavFolderIdOfDevice = (deviceId) => {
    let filteredFolders = this.state.favFolders.filter(folder => folder.deviceIds.includes(deviceId));
    if (filteredFolders.length > 0) {
      return filteredFolders[0].dbId;
    }
  }

  getFavouriteDeviceGroupFolders = async () => {
    const { server } = this.props;
    axios.get(`http://${server}/aioc-rest-web/rest/discovery/getFavouriteDeviceGroupFolders`)
      .then(res => {
        let data = res.data;
        let deviceIds = [].concat.apply([], data.map(x => x.deviceIds));
        this.setState({ favDeviceIds: deviceIds, favFolders: data });
      })
      .catch(err => { console.log(err) })
  }

  handleFav = (deviceId) => {
    const isFav = this.state.favDeviceIds.includes(deviceId);
    if (isFav) {
      this.setState(state => ({
        favDeviceIds: state.favDeviceIds.filter(f => f !== deviceId),
      }));
    }
    else {
      this.setState({ favDeviceId: deviceId });
      this.openModal();
    }
  }

  handleFavSubmit = (deviceId) => {
    this.setState(state => ({
      favDeviceIds: state.favDeviceIds.concat([deviceId])
    }))
  }

  render() {
    const { deviceGroups, favDeviceIds, isFiltered, filteredIds, isModalOpen, favDeviceId } = this.state;
    const { server, sessionId } = this.props;
    return (
      <div className={styles.container}>
        {
          deviceGroups.map((group, index) => {
            return (
              <DeviceGroupCollapse
                getFavFolderId={this.getFavFolderIdOfDevice}
                handleFav={this.handleFav}
                openModal={this.openModal}
                isFiltered={isFiltered}
                filteredIds={filteredIds}
                favDeviceIds={favDeviceIds}
                group={group}
                sessionId={sessionId}
                server={server}
                key={index} />
            );
          })

        }
        <Modal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.closeModal}
          contentLabel="Settings"
          className={styles.settingsModal}
        >
          <FavModal server={server} handleFavSubmit={this.handleFavSubmit} deviceId={favDeviceId} closeModal={this.closeModal} />
        </Modal>
      </div>
    )
  }
}


class DeviceGroupCollapse extends Component {

  state = {
    devices: [],
    expanded: false,
  }

  getDeviceByGroupId = () => {

    const { group, server, sessionId } = this.props;
    axios.get(`http://${server}/aioc-rest-web/rest/discovery/getDevicesOfDeviceGroupByDeviceGroupId/${group.id}`)
      .then(res => {
        let data = res.data;
        this.setState({ devices: data });
      })
      .catch(err => { console.log(err) })
  }

  handleCollapse = () => {
    const { expanded } = this.state;
    if (!expanded) {
      this.getDeviceByGroupId();
    }
    this.setState(prevState => ({
      expanded: !prevState.expanded
    }));
  }

  handleSelect = (event: Event, device) => {
    const { server, sessionId } = this.props;
    event.stopPropagation();
    event.preventDefault();

    newWindow(server, device);
  }

  handleClick = (event) => {
    event.stopPropagation();
    return false;
  }

  handleClickFav = (dbId) => {
    this.props.handleFav(dbId);
  }

  isFav = (device) => {
    const { favDeviceIds } = this.props;
    const isFav = favDeviceIds.indexOf(device.dbId) !== -1;
    return isFav;
  }

  getCollapseIcon = () => {
    const { expanded } = this.state;
    return expanded ? arrowDownImage : arrowRightImage;
  }

  render() {
    const { expanded, devices } = this.state;
    const { server, group, filteredIds, isFiltered, getFavFolderId } = this.props;
    const filteredDevices = isFiltered ? devices.filter(d => filteredIds.includes(d.dbId)) : devices;
    return (
      <div className={styles.deviceGroup} onClick={this.handleCollapse}>
        <div className={styles.title}><img src={this.getCollapseIcon()} />{group.name}</div>
        <div>
          {
            expanded ? filteredDevices.map((device, index) => {
              return (
                <div className={styles.deviceContainer} onClick={this.handleClick} onDoubleClick={(event) => { this.handleSelect(event, device) }} key={index}>
                  <div className={styles.device}>{device.name}</div>
                  <FavIcon server={server} handleClick={this.handleClickFav} deviceId={device.dbId} favFolderId={getFavFolderId(device.dbId)} isFav={this.isFav(device)} />
                </div>
              );
            }
            ) : null
          }
        </div>
      </div>
    )
  }
}

export default DeviceGroupTab;

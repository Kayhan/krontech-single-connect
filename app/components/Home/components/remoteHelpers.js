const { BrowserWindow } = require('electron').remote;

export function newWindow(server, device) {
    let url = "";
    let protocol = "";
    switch (device.accessProtocol) {
      case "CASSANDRA":
      case "RDP":
      case "VNC":
        protocol = "HTTP";
        let deviceBase64Value = btoa(`${device.id}\0c\0singleConnectAuth`);
        url = `${protocol.toString().toLowerCase()}://${server}:80/rdp-ui/#/device/${deviceBase64Value}/&`;
        break;
      case "HTTP":
      case "HTTPS":
        let appUrl = !device.webAppUrl ? "" : (device.webAppUrl[0] === "/" ? device.webAppUrl.substr(0, 1) : device.webAppUrl);
        protocol = device.accessProtocol;
        if (device.portNum > 0) {
          url = `${protocol}://${device.managementIp}:${device.portNum}/${appUrl}`;
        }
        else {
          url = `${protocol}://${device.managementIp}/${appUrl}`;
        }
        break;
      case "TELNET":
      case "SSHv2":
        // protocol = ssl ? "HTTPS" : "HTTP";
        protocol = "HTTP";
        url = `${protocol.toString().toLowerCase()}://${server}/portal-ui/x/tool/single.connect.cli/terminal.jsp?shell=relay&deviceId=${device.id}&`;
        break;
      default:
        break;
    }

    //TODO: Open new window
    let window = new BrowserWindow({
      width: 800,
      height: 800,
      webPreferences: {
        webSecurity: false,
        allowRunningInsecureContent: true,
      }
    });
   
    window.on('closed', () => {
      window = null;
    });
    window.loadURL(`${url}`);
}
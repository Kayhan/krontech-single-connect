import React, { Component } from 'react';
import styles from './ConnectionTypeTab.css';
import axios from 'axios';
import _ from 'lodash';
import FavIcon from '../FavIcon/FavIcon';
import FavModal from '../FavModal/FavModal';
import Modal from 'react-modal';

class ConnectionTypeTab extends Component {

  state = {
    deviceGroups: {},
    favDeviceIds: [],
    favFolders: [],
    isFiltered: false,
    isModalOpen: false,
    favDeviceId: ""
  }

  prevSearchText = "";

  componentWillMount = () => {
    this.getFavouriteDeviceGroupFolders();
  }

  componentDidUpdate = () => {
    this.getDeviceGroups();
  }

  getDeviceGroups = async () => {
    const { searchText, server } = this.props;
    const { isFiltered } = this.state;
    if (searchText === this.prevSearchText) return;
    if (searchText === "" && isFiltered) {
      this.setState({ deviceGroups: {}, isFiltered: false })
    }
    this.prevSearchText = searchText;
    axios.get(`http://${server}/aioc-rest-web/rest/discovery/searchDeviceByKeyword/${searchText}`)
      .then(res => {
        let filter = ["RDP", "VNC", "SSH", "Telnet", "HTTP", "HTTPS"];
        let data = {}
        filter.map(protocol => {
          data[protocol] = res.data.filter(group => group.accessProtocol === protocol);
          return;
        })
        this.setState({ deviceGroups: data, isFiltered: true });
      })
      .catch(err => { /*console.log(err)*/ })
  }


  getFavouriteDeviceGroupFolders = async () => {
    const { server } = this.props;
    axios.get(`http://${server}/aioc-rest-web/rest/discovery/getFavouriteDeviceGroupFolders`)
      .then(res => {
        let data = res.data;
        let deviceIds = [].concat.apply([], data.map(x => x.deviceIds))
        this.setState({ favDeviceIds: deviceIds, favFolders: data });
      })
      .catch(err => { console.log(err) })
  }

  getFavFolderIdOfDevice = (deviceId) => {
    let filteredFolders = this.state.favFolders.filter(folder => folder.deviceIds.includes(deviceId));
    if (filteredFolders.length > 0) {
      return filteredFolders[0].dbId;
    }
  }

  handleFav = (deviceId) => {
    const isFav = this.state.favDeviceIds.includes(deviceId);
    if (isFav) {
      this.setState(state => ({
        favDeviceIds: state.favDeviceIds.filter(f => f !== deviceId),
        favFolders: state.favFolders.map(folder => {
          folder.deviceIds = folder.deviceIds.filter(f => f !== deviceId);
          return folder;
        })
      }));
    }
    else {
      this.setState({ favDeviceId: deviceId });
      this.openModal();
    }
  }

  handleFavSubmit = (deviceId, favFolderId) => {
    this.setState(state => ({
      favDeviceIds: state.favDeviceIds.concat([deviceId]),
      favFolders: state.favFolders.map(folder => {
        if (folder.dbId == favFolderId) {
          folder.deviceIds = folder.deviceIds.concat([deviceId]);
        }
        return folder;
      })
    }))
  }

  openModal = () => {
    this.setState({ isModalOpen: true });
  }

  closeModal = () => {
    this.setState({ isModalOpen: false });
  }

  render() {
    const { deviceGroups, favDeviceIds, favDeviceId } = this.state;
    const { server } = this.props;
    return (
      <div className={styles.container}>
        {
          _.map(deviceGroups, (group, index) => {
            return group.length !== 0 ?
              <DeviceGroupCollapse
                handleFav={this.handleFav}
                getFavFolderId={this.getFavFolderIdOfDevice}
                protocol={index}
                favDeviceIds={favDeviceIds}
                group={group}
                server={server}
                key={index} />
              : null
          })
        }
        <Modal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.closeModal}
          contentLabel="Settings"
          className={styles.settingsModal}
        >
          <FavModal server={server} handleFavSubmit={this.handleFavSubmit} deviceId={favDeviceId} closeModal={this.closeModal} />
        </Modal>
      </div>
    )
  }
}

import arrowDownImage from '../../../../images/arrow_down.png';
import arrowRightImage from '../../../../images/arrow_right.png';
import { newWindow } from '../remoteHelpers';

class DeviceGroupCollapse extends Component {

  state = {
    devices: [],
    expanded: false,
  }

  handleCollapse = () => {
    this.setState(prevState => ({
      expanded: !prevState.expanded
    }));
  }

  handleSelect = (event: Event, device) => {
    const { server } = this.props;
    event.stopPropagation();
    event.preventDefault();
    //TODO: Open new window
    newWindow(server, device);
  }

  handleClick = (event) => {
    event.stopPropagation();
    return false;
  }

  handleClickFav = (event) => {
    event.stopPropagation();
    console.log('fav');
  }

  isFav = (device) => {
    const { favDeviceIds } = this.props;
    const isFav = favDeviceIds.indexOf(device.dbId) !== -1;
    return isFav;
  }

  getCollapseIcon = () => {
    const { expanded } = this.state;
    return expanded ? arrowDownImage : arrowRightImage;
  }

  handleClickFav = (dbId) => {
    this.props.handleFav(dbId);
  }

  render() {
    const { expanded, devices } = this.state;
    const { server, group, protocol, getFavFolderId } = this.props;
    return (
      <div className={styles.deviceGroup} onClick={this.handleCollapse}>
        <div className={styles.title}><img src={this.getCollapseIcon()} />{protocol}</div>
        <div>
          {
            expanded ? group.map((device, index) => {
              return (
                <div className={styles.deviceContainer} onClick={this.handleClick} onDoubleClick={(event) => { this.handleSelect(event, device) }} key={index}>
                  <div className={styles.device}>{device.name}</div>
                  <FavIcon server={server} handleClick={this.handleClickFav} deviceId={device.dbId} favFolderId={getFavFolderId(device.dbId)} isFav={this.isFav(device)} />
                </div>
              );
            }
            ) : null
          }
        </div>
      </div>
    )
  }
}

export default ConnectionTypeTab;

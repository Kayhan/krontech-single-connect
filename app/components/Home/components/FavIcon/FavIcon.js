import React, { Component } from 'react';
import axios from 'axios';

import starIcon from '../../../../images/icon_star.png';
import starIconEmpty from '../../../../images/icon_star_empty.png';

export default class FavIcon extends Component {

  handleClickFav = async (e) => {
    const { handleClick, isFav, favFolderId, deviceId } = this.props;
    if (isFav) {
      await this.removeDeviceFromFav();
    }
    handleClick(deviceId);
  }

  removeDeviceFromFav = () => {
    const {server, favFolderId, deviceId } = this.props;
    console.log(server, favFolderId, deviceId);
    return axios.post(`http://${server}/aioc-rest-web/rest/discovery/removeFavouriteFromFolder`,
      {
        deviceId,
        favFolderId
      })
      .then(res => {
        let data = res.data;
      })
      .catch(err => { console.log(err) })
  }

  getFavIcon = (device) => {
    return this.props.isFav ? starIcon : starIconEmpty;
  }

  render() {
    const { isFav } = this.props;
    return (
      <img onClick={this.handleClickFav} src={this.getFavIcon()} width="20px" />
    )
  }
}
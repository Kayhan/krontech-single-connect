import React, { Component } from 'react';
import styles from './FavModal.css';
import { Formik } from 'formik';
import axios from 'axios';

import withLocalization from '../../../Localization/withLocalization';

class FavModal extends Component {

  state = {
    favFolders: []
  }

  componentWillMount = () => {
    this.getFavouriteDeviceGroupFolders();
  }

  handleSubmit = (values) => {
    const { server, deviceId, handleFavSubmit } = this.props;
    console.log(deviceId, values.name);
    return axios.post(`http://${server}/aioc-rest-web/rest/discovery/addFavouriteToFolder`,
      {
        deviceId,
        favFolderId: values.name
      })
      .then(res => {
        let data = res.data;
        handleFavSubmit(deviceId, values.name);
        this.props.closeModal();
      })
      .catch(err => { console.log(err) })
  }

  getFavouriteDeviceGroupFolders = async () => {
    const { server } = this.props;
    axios.get(`http://${server}/aioc-rest-web/rest/discovery/getFavouriteDeviceGroupFolders`)
      .then(res => {
        let data = res.data;
        this.setState({ favFolders: data });
      })
      .catch(err => { console.log(err) })
  }

  render() {
    const { handleSubmit, closeModal, initialValues, type, saveButton, cancelButton } = this.props;
    const { favFolders } = this.state;
    let values = {
      name: favFolders[0] ? favFolders[0].dbId : "",
    };
    return (
      <div className={styles.modal}>
        <div className={styles.title}>{`Add to Favourites`}</div>
        <div className={styles.form}>
          <Formik
            enableReinitialize={true}
            initialValues={values}
            validate={values => {
              let errors = {};
              return errors;
            }}
            onSubmit={this.handleSubmit}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => {
              return (
                <div className={styles.wrapper}>
                  <form className={styles.form} onSubmit={handleSubmit}>
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="name">Favorite Folder</label>
                      <select
                        className={styles.input}
                        type="text"
                        name="name"
                        id="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      >
                        {
                          favFolders.map((folder, index) => {
                            return <option key={index} value={folder.dbId}>{folder.name}</option>
                          })
                        }
                      </select>
                    </div>
                    <div className={styles.buttons}>
                      <button className={styles.button} type="submit">
                        <img className={styles.buttonImage} src={saveButton} width='90%' />
                      </button>
                      <div className={styles.button}>
                        <img onClick={closeModal} className={styles.buttonImage} src={cancelButton} width='90%' />
                      </div>
                    </div>
                  </form>
                </div>
              )}}
          </Formik>
        </div>
      </div >
    );
  }
}

export default withLocalization(FavModal);
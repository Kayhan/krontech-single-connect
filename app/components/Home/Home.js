// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from '../../constants/routes';
import styles from './Home.css';
import ConnectionTypeTab from './components/ConnectionTypeTab/ConnectionTypeTab';
import DeviceGroupTab from './components/DeviceGroupTab/DeviceGroupTab';
import FavoritesTab from './components/FavoritesTab/FavoritesTab';
import withAuthentication from '../Auth/withAuthentication';
import withLocalization from '../Localization/withLocalization';

import singleConnectLogo from '../../images/single_connect.png';
import logoutImage from '../../images/logout.png';
import searchLogo from '../../images/search.png';

type Props = {};

class Home extends Component<Props> {
  props: Props;

  state = {
    selectedTab: 0,
    searchText: ""
  }

  generateButton = (index) => {
    const { selectedTab } = this.state;
    const { deviceGroupTab,deviceGroupTabSelected, connectionTypeTab, connectionTypeTabSelected, favoritesTab, favoritesTabSelected } = this.props;

    const selectedButtons = [
      deviceGroupTabSelected,
      connectionTypeTabSelected,
      favoritesTabSelected
    ]

    const buttons = [
      deviceGroupTab,
      connectionTypeTab,
      favoritesTab
    ]

    const button = selectedTab === index ? selectedButtons[index] : buttons[index];

    return button;
  }

  generateTab = () => {
    const { selectedTab, searchText } = this.state;
    const { auth: { cookie, server, sessionId } } = this.props;
    const tabs = [
      <DeviceGroupTab className={styles.tabContainer} cookie={cookie} sessionId={sessionId} server={server} searchText={searchText} />,
      <ConnectionTypeTab className={styles.tabContainer} cookie={cookie} sessionId={sessionId} server={server} searchText={searchText} />,
      <FavoritesTab className={styles.tabContainer} cookie={cookie} sessionId={sessionId} server={server} searchText={searchText} />
    ];
    return tabs[selectedTab] ? tabs[selectedTab] : null;
  }

  handleTabSelect = (index) => {
    this.setState({ selectedTab: index })
  }

  handleSearchChange = (event) => {
    // this.setState({ searchText: event.target.value });
    // this.searchText = event.target.value;
  }

  handleLogout = () => {
    this.props.logout();
  }

  handleSearchKeyDown = (event: KeyboardEvent) => {
    if (event.key === "Enter") {
      this.setState({ searchText: event.target.value });
    }
  }

  handleRefresh = () => {
    this.setState({})
  }

  render() {
    const { selectedTab } = this.state;
    const { texts } = this.props;
    return (
      <div className={styles.homeContainer}>
        <div className={styles.top}>
          <div className={styles.logo}>
            <img src={singleConnectLogo} width="80%" />
          </div>
          <div className={styles.logoutButton}>
            <img onClick={this.handleLogout} src={logoutImage} />
          </div>
        </div>
        <div className={styles.tabButtons}>
          <div className={styles.tabButton}>
            <img className={styles.tabImage} onClick={() => { this.handleTabSelect(0) }} src={this.generateButton(0)} />
          </div>
          <div className={styles.tabButton}>
            <img className={styles.tabImage} onClick={() => { this.handleTabSelect(1) }} src={this.generateButton(1)} />
          </div>
          <div className={styles.tabButton}>
            <img className={styles.tabImage} onClick={() => { this.handleTabSelect(2) }} src={this.generateButton(2)} />
          </div>
        </div>
        <div className={styles.searchContainer}>
          <img className={styles.searchIcon} src={searchLogo}/>
          <input onKeyDown={this.handleSearchKeyDown} onChange={this.handleSearchChange} placeholder={texts.search} className={styles.searchBar}/>
        </div>
        <div className={styles.tabContainer}>
          {
            this.generateTab()
          }
        </div>
      </div>
    )
  }
}

export default withLocalization(withAuthentication(Home));
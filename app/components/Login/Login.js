// @flow
import React, { Component } from 'react';
import { Link, Redirect, withRouter } from 'react-router-dom';
import { Formik } from 'formik';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import storage from 'electron-json-storage';

import routes from '../../constants/routes';
import styles from './Login.css';
import SettingsModal from './components/Settings/Settings';
import withAuthentication from '../Auth/withAuthentication';
import withLocalization from '../Localization/withLocalization';

import singleConnect from '../../images/single_connect.png';
import settingsImage from '../../images/settings.png';
import connectionImage from '../../images/connection.png';
import backgroundImage from '../../images/background.png';

type Props = {};

Modal.setAppElement('#root')

class Login extends Component<Props> {
  props: Props;

  state = {
    modalIsOpen: false,
    addresses: [],
  }

  handleSettingsClicked = () => {
    this.openModal();
  }

  handleConnectionsClicked = () => {
    this.props.history.push(routes.CONNECTIONS);
  }

  openModal = () => {
    this.setState({ modalIsOpen: true });
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  onSubmit = (values, { setSubmitting }) => {
    console.log(values);
    setSubmitting(false);
    this.props.login(values);
  }

  initializeStorage = () => {
    // storage.clear();
    storage.get('addresses', (err, data) => {
      if (err) throw err;
      if (data.length >= 0) {
        this.setState({ addresses: data })
      }
    })
  }

  componentWillMount = () => {
    this.initializeStorage();
  }

  render() {
    const { addresses } = this.state;
    const { texts, loginButton } = this.props;
    return (
      <div className={styles.background}>
        <div className={styles.logoWrapper}>
          <img className={styles.logo} src={singleConnect}></img>
        </div>
        <div className={styles.formWrapper}>
          <Formik
            initialValues={{ username: '', password: '', server: addresses.length > 0 ? addresses[0].address : '' }}
            validate={values => {
              let errors = {};
              return errors;
            }}
            onSubmit={this.onSubmit}
            enableReinitialize={true}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => (
                <div className={styles.wrapper}>
                  <form className={styles.form} onSubmit={handleSubmit}>
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="username">{texts.username}</label>
                      <input
                        className={styles.input}
                        type="text"
                        name="username"
                        id="username"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.username}
                      />
                    </div>
                    {errors.username && touched.username && errors.username}
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="password">{texts.password}</label>
                      <input
                        className={styles.input}
                        type="password"
                        name="password"
                        id="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                      />
                    </div>
                    {errors.password && touched.password && errors.password}
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="server">{texts.server}</label>
                      <select
                        className={styles.select}
                        type="server"
                        name="server"
                        id="server"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.server}
                        defaultValue={addresses[0]}
                      >
                        {
                          this.state.addresses.map((a, index) => {
                            return (
                              <option value={a.address} key={index}>{a.address}</option>
                            )
                          })
                        }
                      </select>
                    </div>
                    <button className={styles.button} type="submit" disabled={isSubmitting}>
                      <img src={loginButton}></img>
                    </button>
                  </form>
                </div>
              )}
          </Formik>
        </div>
        <div className={styles.buttonsWrapper}>
          <div className={styles.buttonWrapper}>
            <img onClick={this.handleSettingsClicked} className={styles.buttonLogo} src={settingsImage} />
            <label className={styles.settingsLabel}>{texts.settings}</label>
          </div>
          <div className={styles.buttonWrapper}>
            <img className={styles.buttonLogo} onClick={this.handleConnectionsClicked} src={connectionImage} />
            <label className={styles.connectionsLabel}>{texts.connections}</label>
          </div>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel="Settings"
          className={styles.settingsModal}
        >
          <SettingsModal closeModal={this.closeModal} />
        </Modal>
      </div>
    )
  }
}

export default   withLocalization(withAuthentication(Login));
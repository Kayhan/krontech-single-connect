import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import styles from './Settings.css';
import { Formik } from 'formik';
import storage from 'electron-json-storage';

import { changeLanguage, getLanguage } from '../../../../actions/localization';
import withLocalization from '../../../Localization/withLocalization';

import cancelImage from '../../../../images/cancel_button.png';
import saveImage from '../../../../images/save_button.png';

type Props = {};

class SettingsModal extends Component<Props> {

  handleSubmit = async ({ language }, { setSubmitting }) => {

    console.log(language);
    await this.props.changeLanguage(language);
    setSubmitting(false);
    this.props.closeModal();
  }

  componentWillMount = async () => {
      await this.props.getLanguage();
  }

  render() {

    const { language, closeModal, saveButton, cancelButton } = this.props;
    return (
      <div className={styles.background}>
        {
          language ?
            <Formik
              initialValues={{ language: language }}
              validate={values => {
                let errors = {};
                return errors;
              }}
              onSubmit={this.handleSubmit}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                /* and other goodies */
              }) => (
                  <form className={styles.form} onSubmit={handleSubmit}>
                    <div className={styles.inputWrapper}>
                      <label className={styles.label} htmlFor="language">Language</label>
                      <select
                        className={styles.select}
                        type="language"
                        name="language"
                        id="language"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.language}
                      >
                        <option value="en-US">en-US</option>
                        <option value="tr-TR">tr-TR</option>
                      </select>
                    </div>
                    <div className={styles.buttonsWrapper}>
                      <button className={styles.button} type="submit" disabled={isSubmitting}>
                        <img src={saveButton} width='100%'>

                        </img>
                      </button>
                      <button onClick={closeModal} className={styles.button} type="button" disabled={isSubmitting}>
                        <img src={cancelButton} width='100%'></img>
                      </button>
                    </div>
                  </form>
                )}
            </Formik> : null
        }
      </div>
    );
  }
}

export default withLocalization(SettingsModal);
import React, { Component } from 'react';
import { connect } from 'react-redux';
// import localization from '../../constants/localization.json';
import { changeLanguage, getLanguage } from '../../actions/localization';

import addButtonTR from "../../images/tr/add_button.png";
import cancelButtonTR from "../../images/tr/cancel_button.png";
import deleteButtonTR from "../../images/tr/delete_button.png";
import connectionTypeTabTR from "../../images/tr/connection_type_tab.png";
import connectionTypeTabSelectedTR from "../../images/tr/connection_type_tab_selected.png";
import deviceGroupTabTR from "../../images/tr/device_group_tab.png";
import deviceGroupTabSelectedTR from "../../images/tr/device_group_tab_selected.png";
import favoritesTabTR from "../../images/tr/favorites_tab.png";
import favoritesTabSelectedTR from "../../images/tr/favorites_tab_selected.png";
import loginButtonTR from "../../images/tr/login_button.png";
import saveButtonTR from "../../images/tr/save_button.png"
import addButton from "../../images/add_button.png";
import cancelButton from "../../images/cancel_button.png";
import deleteButton from "../../images/delete_button.png";
import connectionTypeTab from "../../images/connection_type_tab.png";
import connectionTypeTabSelected from "../../images/connection_type_tab_selected.png";
import deviceGroupTab from "../../images/device_group_tab.png";
import deviceGroupTabSelected from "../../images/device_group_tab_selected.png";
import favoritesTab from "../../images/favorites_tab.png";
import favoritesTabSelected from "../../images/favorites_tab_selected.png";
import loginButton from "../../images/login_button.png";
import saveButton from "../../images/save_button.png";

export default function (Component) {
    class WithLocalization extends Component {
        render() {
            return <Component {...this.props} />
        }
    }

    WithLocalization.displayName = `WithLocalization(${getDisplayName(Component)})`;
    return connect(mapStateToProps, mapDispatchToProps)(WithLocalization);
}

function mapStateToProps(state) {
    const { language } = state.language;
    let lang = language ? language : "en-US";
    return {
        language: lang,
        route: lang === "en-US" ? "" : "tr",
        ...localization[lang]["images"],
        "texts": localization[lang]["texts"]
    };
}

function mapDispatchToProps(dispatch) {
    return {
        changeLanguage: (language) => {
            dispatch(changeLanguage(language));
        },
        getLanguage: () => {
            dispatch(getLanguage());
        }
    }
}

function getDisplayName(Component) {
    return Component.displayName || Component.name || 'Component';
}

const localization = {
    "tr-TR": {
        "images": {
            "addButton": addButtonTR,
            "cancelButton": cancelButtonTR,
            "deleteButton": deleteButtonTR,
            "connectionTypeTab": connectionTypeTabTR,
            "connectionTypeTabSelected": connectionTypeTabSelectedTR,
            "deviceGroupTab": deviceGroupTabTR,
            "deviceGroupTabSelected": deviceGroupTabSelectedTR,
            "favoritesTab": favoritesTabTR,
            "favoritesTabSelected": favoritesTabSelectedTR,
            "loginButton": loginButtonTR,
            "saveButton": saveButtonTR
        },
        "texts": {
            "settings" : "Ayarlar",
            "connections" : "Baglantilar",
            "username" : "Kullanici Adi",
            "password" : "Sifre",
            "server" : "Server",
            "search" : "Cihaz Ara",
            "newFolder" : "Yeni Dosya"
        }
    },
    "en-US": {
        "images": {
            "addButton": addButton,
            "cancelButton": cancelButton,
            "deleteButton": deleteButton,
            "connectionTypeTab": connectionTypeTab,
            "connectionTypeTabSelected": connectionTypeTabSelected,
            "deviceGroupTab": deviceGroupTab,
            "deviceGroupTabSelected": deviceGroupTabSelected,
            "favoritesTab": favoritesTab,
            "favoritesTabSelected": favoritesTabSelected,
            "loginButton": loginButton,
            "saveButton": saveButton,
        },
        "texts": {
            "settings" : "Settings",
            "connections" : "Connections",
            "username" : "Username",
            "password" : "Password",
            "server" : "Server",
            "search" : "Search Devices",
            "newFolder" : "New Folder"
        }
    }
}
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect, withRouter } from 'react-router-dom';
import routes from '../../constants/routes';
import { login, logout } from '../../actions/auth';

export default function (Component) {
  class WithAuthentication extends Component {

    checkAndRedirect = () => {
      const { auth: { auth }, history } = this.props;
      if (!auth && history.location.pathname !== routes.LOGIN) {
        history.push(routes.LOGIN);
      } else if (auth && history.location.pathname === routes.LOGIN) {
        history.push(routes.HOME);
      }
    }

    componentDidUpdate() {
      this.checkAndRedirect();
    }

    componentDidMount() {
      this.checkAndRedirect();
    }

    render() {
      return <Component {...this.props} />
    }
  }
  WithAuthentication.displayName = `WithAuthentication(${getDisplayName(Component)})`;
  return connect(mapStateToProps, mapDispatchToProps)(withRouter(WithAuthentication));
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    error: state.error
  }
}

function mapDispatchToProps(dispatch) {
  return {
    login: function ({ username, password, server }) {
      dispatch(login(username, password, server));
    },
    logout: () => {
      dispatch(logout());
    }
  }
}

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}
